const phin = require("phin")
const cheerio = require("cheerio")
const inquirer = require("inquirer")
const open = require("open")

if (!process.argv[2]) {
    console.log("I need the ID of the game you wanna play!")
    console.log("Usage: node ./launch-game.js [Game ID]")
    process.exit(1)
}

async function login(username, password) {
    let loginpage = await phin({ url: "https://www.brick-hill.com/login" })
    let xsrftoken = loginpage.body.toString().match(/<meta name="csrf-token" content="(.+)">/)[1]

    let login = await phin({
        url: "https://www.brick-hill.com/login",
        method: "POST",
        headers: {
            "Cookie": loginpage.headers["set-cookie"].join("; ")
        },
        form: {
            _token: xsrftoken,
            username: username,
            password: password
        }
    })
    if (login.headers.location === "https://www.brick-hill.com") {
        return login.headers["set-cookie"].join("; ")
    } else {
        throw new Error("Invalid login credentials")
    }
}

async function getIPandPort(setID) {
    let rawdata = await phin(`https://www.brick-hill.com/play/${setID}`)
    let $ = cheerio.load(rawdata.body)
    let pbutton = $('play-button')
    return {
        ip: Buffer.from(pbutton['0'].attribs.ip.split("").reverse().join(""), 'base64').toString('ascii'),
        port: pbutton['0'].attribs.port
    }
}

async function getGameToken(cookie, setID) {
    let rawdata = await phin({
        url: `https://api.brick-hill.com/v1/auth/generateToken?set=${setID}`,
        method: "GET",
        headers: { "Cookie": cookie }
    })
    let data = JSON.parse(rawdata.body)
    if (data.error) throw new Error(data.error) // this should probably never happen
    return data.token
}

async function launchGame(setID) {
    console.log("Log into your Brick Hill account.")

    // Get username and password from the user
    let { username, password } = await inquirer.prompt([
        { type: 'input', name: 'username', message: 'Username?' },
        { type: 'password', name: 'password', mask: '*', message: 'Password?' }
    ])

    // Get the cookie by logging in with the username and password
    let cookie = await login(username, password)

    // Get the IP, port, and game token
    let result = await Promise.all([
        getIPandPort(setID),
        getGameToken(cookie, setID)
    ])

    // extraction
    let { ip, port } = (typeof result[0] === "object" ? result[0] : result[1])
    let gameToken = (typeof result[1] === "string" ? result[1] : result[0])

    // brickhill.legacy://client/[TOKEN]/[IP]/[PORT]
    await open(`brickhill.legacy://client/${gameToken}/${ip}/${port}`)
}

launchGame(process.argv[2])